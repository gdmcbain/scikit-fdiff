#!/usr/bin/env python
# coding=utf-8

import json

import numpy as np
import pytest
import xarray as xr
import yaml
from path import tempdir

from skfdiff import Container, Model, Simulation, retrieve_container


@pytest.fixture
def heat_model():
    model = Model(
        evolution_equations="k * dxxT",
        unknowns="T",
        parameters="k",
        boundary_conditions="periodic",
    )
    return model


@pytest.fixture
def fields(heat_model):
    x = np.linspace(0, 10, 50, endpoint=False)
    T = np.cos(x * 2 * np.pi / 10)
    initial_fields = heat_model.Fields(x=x, T=T, k=1)

    return initial_fields


@pytest.fixture
def simul(heat_model, fields):
    simul = Simulation(
        heat_model,
        fields.copy(),
        dt=0.5,
        tmax=2,
        tol=1e-1,
        id="test_skfdiff_containers",
    )
    return simul


def test_containers_coerce(simul, fields):
    container_path = tempdir()
    simul.attach_container(container_path)
    simul.run()


def test_containers_last(simul, fields):
    with pytest.raises(ValueError):
        simul.attach_container(None, save="")
    container_path = tempdir()
    simul.attach_container(container_path, save="last")
    simul.run()
    assert simul.container.data.t.size == 1
    assert simul.container.data == simul.fields


def test_containers_attached_memory(simul, fields):
    simul.attach_container(None)
    simul.run()

    assert simul.container.data.isel(t=0) == fields
    assert simul.container.data.isel(t=-1) == simul.fields


def test_containers_attached_ondisk(simul, fields):
    container_path = tempdir()
    container = simul.attach_container(container_path)
    simul.run()
    container.merge()

    assert simul.container.data.isel(t=0) == fields
    assert simul.container.data.isel(t=-1) == simul.fields

    assert (
        xr.open_dataset(container_path / simul.id / "merged_data.nc")
        == simul.container.data
    )
    with pytest.raises(FileExistsError):
        Container(path=container_path / simul.id, force=False, mode="w")


@pytest.mark.parametrize("lazy", (True, False))
def test_containers_retrieve_all(simul, lazy):
    container_path = tempdir()
    simul.attach_container(container_path)
    simul.run()
    container = retrieve_container(container_path / simul.id, lazy=lazy, isel="all")
    assert container.data == simul.container.data
    assert container.metadata == simul.container.metadata


@pytest.mark.parametrize("lazy", (True, False))
def test_containers_retrieve_last(simul, lazy):
    container_path = tempdir()
    simul.attach_container(container_path)
    simul.run()
    container = retrieve_container(container_path / simul.id, lazy=lazy, isel="last")
    assert container.data == simul.container.data.isel(t=-1)
    assert container.metadata == simul.container.metadata


@pytest.mark.parametrize("lazy", (True, False))
def test_containers_retrieve_list(simul, lazy):
    container_path = tempdir()
    simul.attach_container(container_path)
    simul.run()
    container = retrieve_container(container_path / simul.id, lazy=lazy, isel=[0, 1, 2])
    assert container.data == simul.container.data.isel(t=[0, 1, 2])
    assert container.metadata == simul.container.metadata


@pytest.mark.parametrize("lazy", (True, False))
def test_containers_retrieve_incomplete(simul, lazy):
    container_path = tempdir()
    simul.attach_container(container_path)
    next(simul)
    simul.container.flush()
    next(simul)
    simul.container.flush()
    container = retrieve_container(container_path / simul.id)
    assert container.data == simul.container.data
    assert container.metadata == simul.container.metadata


@pytest.mark.parametrize("lazy", (True, False))
def test_containers_merge(simul, lazy):
    container_path = tempdir()
    simul.attach_container(container_path)
    next(simul)
    simul.container.flush()
    next(simul)
    simul.container.flush()
    sliced_data = simul.container.data.load().copy()
    Container.merge_datafiles(container_path / simul.id)
    fields = xr.open_dataset(container_path / simul.id / "merged_data.nc")
    assert fields == sliced_data


@pytest.mark.parametrize("mode", ("w", "a"))
def test_containers_meta_set(mode):
    cont = Container(None, mode)
    cont.metadata = dict(test="foo")
    assert cont.metadata["test"] == "foo"

    container_path = tempdir()
    cont = Container(container_path / "test_meta", mode)
    cont.metadata = dict(test="foo")

    with open(container_path / "test_meta" / "metadata.yml", "r") as f:
        pars = yaml.load(f)
    assert cont.metadata["test"] == "foo"
    assert cont.metadata["test"] == pars["test"]
    cont = Container(container_path / "test_meta", "r")
    cont.metadata = dict(test="foo")
    (container_path / "test_meta" / "metadata.yml").remove()
    assert cont.metadata is None
    assert cont.data is None

    cont = Container(None, "r")

    cont.metadata["test"] = "foo"


def test_containers_repr():
    cont = Container(None)
    str(cont)
